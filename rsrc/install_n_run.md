# e-bid ui

## Intro

This project implements the client side of the e-bid application.
It is built with the [React.js Framework](https://reactjs.org/).

## Security

For security reasons, all
Http requests involving passwords are **base-64 encoded** and all pages
that require some kind of authority, check the localStorage of the browser
to ensure that the user is authorized or else redirect that user to the appropriate
page based on the role of that user.

## Site-Map

Based on the user **role** the following pages are displayed

### Role: `Guest`

This user is not yet logged in and is able to access the following pages.

#### Welcome Page

On launching, the application renders the Welcome Page where popular, based on the number of bids,
products are displayed. This product list is retrieved page by page in order to achieve maximum performance
while the user is able to pick the page of his preference and is able
to navigate to the following sections.

* **sign-up**
* **sign-in**
* **search**

##### SignUp

This page displays a nice form to fill with personal details necessary for an account to be created.
This form includes the following constraints:

* **user name** shall not exist already
* **password** should be strong(at least least 8 characters long including a number, a lower and an upper case letter)
* **retype** password should match the prototype
* **vat** number must contain 6 digits
* **e-mail** should contain an '@'

The form is validated and if necessary, the user is asked to modify some fields.
After a successful sign-up the user is informed that his registration is pending approval from the administrator.
The password is encoded and send to the server-side component of this application.

##### SignIn

This page asks for the user's credentials. The user might be rejected on the following cases

* Incorrect password or user name
* User's status is `pending` so this user is unable to login until approved from the admin.

##### Search

This page allows the user to search for a product either

* by **category** or
* by **name/description** and/or maximum **price**

The result list is displayed in the form of cards and the
user is able to view each product of the list but not make any bid on it, except if logged in.

> If that is the case, the user can both **rate** the product and **make a bid**, larger than the largest one
until now or, if there is none made yet, the first bid.
If the bid made is equal to the actual buy price defined for this product, the user has successfully purchased it
and is now redirected to the chat page and prompted to start or continue a conversation with the seller.

### Role: `Admin`

A user authorized as administrator is immediately redirected to the
the Admin Page. This page is consists of
the **Panel**, a paginated table with one row for each client
having as columns his username, status and a link to his page
where the admin can view his private info and either accept or reject
his registration request if it is pending.
The Panel also allows the admin to export all products ever offered
on this site in [JSON](https://www.json.org/) format.

### Role: `Client`

Right after logging in, a client is redirected to the **Homepage** which
renders all products **recommended** for that particular client based on his
ratings and views, using [collaborative filtering](https://en.wikipedia.org/wiki/Collaborative_filtering)
with [pearson correlation](https://en.wikipedia.org/wiki/Pearson_correlation_coefficient)
meaning that based on the ratings of the most similar
to this client clients(**neighbors**)
it renders products of their preference while taking into consideration
the **similarity degree** and the **average product rating** of each neighbor.

Once logged in the navigation bar is adjusted enabling the client to navigate
to the following pages

#### Messenger

This page displays all conversations made with other clients.
The only way to start a conversation with another client
is by buying a product from that client's offer list.

In order to view new messages, a client must reload the chat or send a message
so that the chat automatically reloads. Notice that once a chat is modified,
it is the only one fetched from the server. The rest of the conversation list
remains as is. Each conversation contains a time stamp of the latest message, so
that they are ordered with the most recent ones on top. This time stamp
is displayed on the right of the peer's name.

Each message is also followed by a time stamp too, which is displayed on top of it.

#### Product list

This type of pages consist of the client's

* Offers
* Purchases
* Bids

and are accessible through the **Settings**.  
Each of these pages displays a list of products filtered as Active or Expired.
Active offers can be edited while Expired only viewed.
Purchases can be viewed only.

The **Offers** page also allows the client to add a
new product with the following constraints

* First bid must be smaller than Buy price
* Start time shall be later than present and
end-time must be later that start time.

Note that editing an offer is now allowed after either a bid has been made or the
auction has began and the same constraints for adding a product also apply on editing one.

>Editing is also applied to the profile of the client,
having the same constraints as the [sign-up](###SignUp) page.

After clicking on viewing a product, a request is sent from
the application in order to record that view. If the client is
not also the owner then the same options as for the [search-page](###Search)
apply here.

## Run it

To run it make sure all dependencies
mentioned in the package.json are included. If not, run

```bash
    npm install
```

anywhere under the `view` directory and the `node_modules`
folder will be updated.

After all dependencies are installed, run

```bash
    npm start
```

to kick start the application.

Make sure you try it out.

## Epilogue

That was it, make sure you try this app out.
Feel free to comment or even contribute to this project.
Fork this repository make some changes and issue a pull request.
It will be reviewed as soon as possible and you will get a response.
Thanks for reading.
