import React from 'react'

function Icon(props){
    return (props.icon === undefined ? null :
            <div className="input-group-prepend" >
                <span className="input-group-text" style={{backgroundColor : "white"}}><i className={"fa fa-" + props.icon}></i></span>
            </div>)
}

export default Icon