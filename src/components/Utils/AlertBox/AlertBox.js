import React from 'react'
import './alert.css'

function AlertBox(props){
    
    const alerts = props.state.alerts && props.state.alerts.map((alert, index) => (
            <div key={index} className={"alert alert-" + alert.type + " alert-box mx-auto my-5px"}>
                <span className="alert-message">{alert.message}</span> 
                <i className="fa fa-times alert-remove" onClick = {() => props.handler(index)}></i> 
            </div>))

    return (<div>{alerts}</div>)
}

export default AlertBox