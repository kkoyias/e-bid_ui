import React from 'react'
import {Row, Col} from 'react-bootstrap'
import InputIcon from './InputIcon'
import config from "./config"

function InputGenerator(props){

    // convert a list of form fields to a list of html input elements inside a Column
    const toInput = (item) => {
        const commonFields = {
            required : true,
            style : {width : "80%"},
            className: "form-control",
            onChange : (e) => {console.log(props.id.state) ; props.id.setState({errors: [], [e.target.name] : e.target.value})},
        }
        return (
            <Col key={item.label} className = {"col-md-" + item.size}>
            <label><strong>{item.label}</strong></label><br/>
            <div className="input-group">
                <InputIcon icon={item.icon}/>
                {React.createElement("input", {...item.input, ...{value : props.id.state[item.input.name] || ""},...commonFields})}
            </div>
            </Col>
        )
    }

    // convert a list of lists to a list of Rows, each containing a list of Columns
    const getRow = (list, index) => 
        <div key={index}>
            <Row>
                {list.map(toInput, props.id)}
            </Row><br/>
        </div> 

    return props.list.map(getRow)
}

function Load(){
    return <p className="loading"><i className="fa fa-refresh fa-spin"></i> Loading</p>
}

function formatDate(date){
    return ("0" + date.getHours()).slice(-2) + ":" + ("0" + date.getMinutes()).slice(-2) + "\t" + 
    ("0" + date.getFullYear()).slice(-2) + "-" + ("0" + date.getMonth()).slice(-2) + "-" + ("0" + date.getDay()).slice(-2)
}

function itemIsActive(item){
    return new Date() < new Date(item.endsAt) && !item.sold
}

// if user is not authorized to access a page, redirect that user accordingly
function authorizeUser(role, id){

    const  myRole = localStorage.getItem('userRole') || "GUEST", myId = parseInt(localStorage.getItem('userId'))

    if(role !== myRole)
        return config.home[myRole]
    else if(id)
        return myId ? "" : config.home[myRole]
    else
        return ""
}

const getProducts = ((item, index) => {
    return {
        id : item.id, 
        name : item.name.slice(0,1).toUpperCase() + item.name.slice(1), 
        location : item.location ? item.location.address.slice(0, item.location.address.indexOf(",")) : "Unknown location", 
        description : item.description,
        sold: item.sold
    }
})

export {InputGenerator, Load, authorizeUser, formatDate, itemIsActive, getProducts}