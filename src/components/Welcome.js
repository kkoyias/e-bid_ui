import React from 'react'
import ReactDOM from 'react-dom'
import { Link } from "react-browser-router"
import { Load, itemIsActive, getProducts } from './Utils/Utils'
import AlertBox from './Utils/AlertBox/AlertBox'
import Listing from "./Results/Listing"

const config = {prodsPerRow : 4, useRows : 2}

class Welcome extends React.Component{
    constructor(){
        super()
        this.state = {products : [], loading : true, alerts: []}
    }

    // on mounting fetch trending products to display
    componentDidMount(){
        ReactDOM.findDOMNode(this).addEventListener('click', () => this.setState({alerts: []}))
        const url = "http://localhost:8080/api/items"
        fetch(url)
            .then(response => response.json())
            .then(data => this.setState({
                loading: false,

                // create product list out of the result, omitting expired offers
                products : Array.isArray(data) ? 
                    data.filter(itemIsActive).map(getProducts) : data._embedded.items.filter(itemIsActive).map(getProducts)
            }))
            .catch(error => {
                this.setState({
                    loading: false,
                    alerts: [{
                        message: "Failed to load resource. Please check your connection",
                        type: "danger"
                    }]
                })
                console.error("Error on welcome fetch: ", error)
            })
    }

    // display the trending products in a tabular fashion, until having retrieved them successfully, display "Loading..."
    render(){
        console.log(this.state)

        return(
            <div style={{backgroundColor : "ghostwhite"}}>
                <div id="welcome-image" className="display-4">
                    <div id="welcome-text">
                        <h1 style={{fontSize : "0.75em"}}>Welcome to e-bid</h1>
                        <h5>Make an offer they can't refuse</h5><hr/>
                        <Link to="/join/0">
                            <button className="btn btn-outline-dark btn-sm">
                            <i className="fa fa-gavel" style={{fontSize: "15px"}}></i> Get Started</button>
                        </Link>
                    </div>
                </div><br/>
                <h3 className="main-header">Popular right now</h3><br/>
                <div>{this.state.loading ?  <Load/> : <Listing mode="check" list={this.state.products} config={config}/>}</div>
                <AlertBox state={this.state} handler={() => this.setState({alerts: []})} />
            </div>
        )
    }
}

export default Welcome