import React from "react"
import { Container} from "react-bootstrap"
import { Load, itemIsActive, getProducts } from "../Utils/Utils"
import Listing from "../Results/Listing"
import ByCategory from "./ByCategory"

const config = {prodsPerRow : 4, useRows : 2}
const filterResults = (item => item.seller.id !== parseInt(localStorage.getItem('userId')) && itemIsActive(item))

class Search extends React.Component {
  constructor() {
    super()
    this.state = {description: "", results: []}
    this.onSubmit = this.onSubmit.bind(this)
    this.search = this.search.bind(this)
    this.onClick = this.onClick.bind(this)
  }

  // handle a category click
  onClick(choice){
    console.log(choice)

    const categoriesUrl = "http://localhost:8080/api/items/search?categoryID=" + choice.id  
    this.search(categoriesUrl, {isText: false, content: choice.name})
  }

  // handle a full-text search submit
  onSubmit(event){

    event.preventDefault()
    const priceParameter = this.state.price ? "price=" + this.state.price + "&" : ""
    const descriptionParameter = this.state.description ? "description=" + this.state.description : ""
    const queryUrl = "http://localhost:8080/api/items/search?" + priceParameter + descriptionParameter
    this.search(queryUrl, {isText: true, content: this.state.description})
  }

  // fetch data from the appropriate API defined under type, and update the corresponding value of state
  search(url, {isText, content}){
    console.log(url, content)

    this.setState({submitted: true, loading: true, query : {isText: isText, value: content}})
    fetch(url)
        .then(response => response.json())
        .then(data => this.setState({
            loading: false,

            // transform results to the appropriate format and reject own products
            results : data.filter(filterResults).map(getProducts)
        }))
        .catch(error => {
          this.setState({loading: false})
          console.error( `Error on search for : "${content}(text? ${isText})"`, error)
        })
  }

  render() {
    console.log(this.state)

    return (
      <div>
        {/* display a categories Dropdown Button and a Search bar*/}
        <Container fluid id="search-container" className="flex-container">
            <form id="search-bar" className="form-inline md-form" onSubmit={this.onSubmit}>
              <ByCategory onClick={this.onClick}/><br/>
              <input className = "form-control mr-sm-2" autoFocus type = "text"
                  placeholder = "Search by description or name..." aria-label = "Search" 
                  onChange = {(event) => this.setState({description : event.target.value})}
                  value = {this.state.description}
              />
              <input className="form-control mr-sm-2" type="number" min="1" 
                  placeholder="Max price..."
                  onChange = {(e) => this.setState({price: e.target.value})}
              />
              <button className="btn btn-outline-dark btn-rounded btn-md my-0" type="submit"> 
                  Search
              </button>
            </form>
        </Container>

        {/* Show results on submit*/}
        {this.state.submitted &&
          <Container className="text-center" fluid id="search-results">
            <h1>{this.state.query.isText ? 
              "Results" + (this.state.query.value && " for '" + this.state.query.value + "'")  
              : 
              this.state.query.value}
            </h1>
            {this.state.loading ?  <Load/> : <Listing mode="check" list={this.state.results} config={config}/>}
            <a href="#search-container">
              <button id="back-to-top" className="btn btn-outline-primary btn-md">Back to top</button>
            </a>
          </Container>
        }
      </div>
    )
  }
}

export default Search