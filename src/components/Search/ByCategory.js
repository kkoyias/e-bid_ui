import React from 'react'
import {Dropdown, DropdownButton} from "react-bootstrap"
import {getCategoryHierarchy} from "../User/AddProduct/Categories"

class ByCategory extends React.Component{
    constructor(props){
        super(props)
        this.state = {categories: []}
        this.handleChoice = this.handleChoice.bind(this)
        this.convertToDropdown = this.convertToDropdown.bind(this)
    }

    componentDidMount(){
        getCategoryHierarchy(this)
    }

    // on category selected, pass category above
    handleChoice(subCategory){
        document.getElementById("search-category-button").innerHTML = subCategory.name
        this.props.onClick(subCategory)
    }

    // import all categories and their sub-categories and convert them to a dropdown menu
    convertToDropdown(category, index){
        return(
            <div key={index}>
                <Dropdown.Header>
                    {category.name}
                </Dropdown.Header>
                {category.subCategories.map(
                    (sub, ind) => <Dropdown.Item onClick={() => this.handleChoice(sub)} key={sub.id}>{sub.name}</Dropdown.Item>
                )}
            </div>
        )
    }

    render(){
        const items = this.state.categories.map(this.convertToDropdown)

        return(
            <DropdownButton title="Category" id="search-category-button">
                {items}
            </DropdownButton>
        )
    }
}

export default ByCategory