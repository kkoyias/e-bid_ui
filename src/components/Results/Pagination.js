import React from 'react'
import ReactPaginate from 'react-paginate'

function Paginate(props){

    // set starting and ending item, when a page is selected
    function handlePageChange(event){
        const start = event.selected * props.itemsPerPage, end = start + props.itemsPerPage
        this.setState({start : start, end : end})
    }

    return(
        <div className = "div-center">
            <ReactPaginate
                pageCount = {Math.ceil(props.total/props.itemsPerPage)}
                pageRangeDisplayed = {5}
                marginPagesDisplayed = {5}
                initialPage ={0}
                onPageChange = {handlePageChange.bind(props.component)}
                containerClassName = "list-group list-group-horizontal"
                pageClassName = "list-group-item"
                activeClassName = "list-group-item list-group-item-primary" 
                previousClassName = "list-group-item"
                breakClassName = "list-group-item" // the ellipsis
                nextClassName = "list-group-item"
            />
        </div>
    )
}

export default Paginate