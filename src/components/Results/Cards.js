import React from 'react'
import {Row, Col, Card} from 'react-bootstrap'

const gridRows = 12
const defaultImages = ["https://cdn.pixabay.com/photo/2017/02/20/08/47/professions-2081837_960_720.jpg",
                        "https://cdn.pixabay.com/photo/2017/02/20/08/47/professions-2081832__340.jpg",
                        "https://cdn.pixabay.com/photo/2017/02/16/12/00/professions-2071310__340.jpg",
                        "https://cdn.pixabay.com/photo/2017/02/16/12/00/professions-2071311__340.jpg",
                        "https://cdn.pixabay.com/photo/2017/02/16/12/00/professions-2071312__340.jpg",
                        "https://cdn.pixabay.com/photo/2017/02/20/08/47/professions-2081831__340.jpg"]

function Cards(props){
    const meta = {"edit" : {link : "/products", message: "Edit"}, "check" : {link : "/items", message: "Check it out"}}
    const {link, message} = meta[props.mode], limit = 22

    // convert an object representing a product to a Column containing a Card describing that product
    function getCard(item, index){
        const description = item.description.length > limit ? item.description.slice(0, limit) + "..." : item.description
        return (
            <Col key = {index} className={"col-sm-" + gridRows/props.state.conf.prodsPerRow}>
                <Card className = "product-card">
                    {item.recommended && <Card.Header>Recommended</Card.Header>}
                    <Card.Body>
                        <Card.Img 
                            variant="top" 
                            src={item.img || defaultImages[index % defaultImages.length]}
                            alt={item.id + " image"}/>
                        <Card.Title className="card-title">{item.name}</Card.Title>
                        <Card.Subtitle className="mb-2 text-muted subtitle">
                            {item.location || "Unknown location"}
                        </Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Link id="card-link" href={link + "/" + item.id}>{message}</Card.Link>
                    </Card.Body>
                </Card>
            </Col>
        )
    }

    // given a list of Columns, create a list of Rows having '$prodsPerRow' Columns each
    function getRows(list){
        let rv = []
        for(let i = 0; i < list.length; i += props.state.conf.prodsPerRow)
            rv.push(
            <div style = {{textAlign : "center"}} key={i}>
                <Row>{list.slice(i, i + props.state.conf.prodsPerRow)}</Row><br/>
            </div>)
        return rv   
    }

    return getRows(props.state.list.slice(props.state.start, props.state.end).map(getCard))
}

export default Cards