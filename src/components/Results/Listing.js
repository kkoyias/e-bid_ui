import React from 'react'
import Pagination from './Pagination'
import Cards from "./Cards"

// this component keeps track of state while Cards and Pagination display and update it
class Listing extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            conf : this.props.config,
            list : this.props.list,
            start : 0,
            end: 0
        }
    }

    componentWillReceiveProps(props){
        this.setState({list: props.list})
    }

    render(){
        console.log(this.state)
        return (
            <div>
                <Cards mode={this.props.mode} component={this} state={this.state}/>
                {this.state.list && this.state.list.length > 0 &&
                    <Pagination
                        itemsPerPage = {this.state.conf.prodsPerRow * this.state.conf.useRows}
                        total = {this.state.list.length}
                        component = {this}
                    /> }
            </div>
        )
    }
}

export default Listing