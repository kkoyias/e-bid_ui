import React from "react"
import { BrowserRouter as Router } from "react-router-dom"
import { Switch, Route } from "react-router-dom"
import NavigationBar from "./NavigationBar"
import Welcome from "./Welcome"
import Homepage from "./Homepage";
import Search from "./Search/Search"
import SignIn from "./Guest/SignIn"
import SignUp from "./Guest/SignUp"
import {Message} from "./Guest/ResetFunctions"
import Forgot from "./Guest/Forgot"
import Panel from "./Admin/Panel"
import UserInfo from "./Admin/UserInfo"
import About from "./Footer/About"
import Footer from "./Footer/Footer"
import AddProduct from "./User/AddProduct/AddProduct"
import Actions from "./User/Actions/Actions"
import Chat from "./User/Chat/Chat"
import ProductView from "./ProductView/ProductView"

// App is the main component and the 'single source of truth' for this application, it kick starts the app and observes state
class App extends React.Component {
  // Initialize App, user is not logged in at first
  constructor() {
    super()
    this.state = {
      user: {
        id : null,
        type : null
      }
    }
  }

  // display a navBar with a series of choices leading to different pages/components
  render() {
    return (
      <Router>
        <div className="App">
          <NavigationBar userGroup={this.state.user.type} /> {/*NavigationBar*/}
          {/*localStorage.getItem('token') && 
          <h1 className="note note-left">Logged in as: <span>{localStorage.getItem('userName')}</span></h1>*/}
          
          <Switch>
            <Route path="/" exact component={Welcome} />
            <Route path="/home" exact component={Homepage}/>
            <Route path="/search" exact component={Search}/>
            <Route path="/login" exact component={SignIn} />
            <Route path="/pending" render={() => <Message type="pending"/>} />
            <Route path="/join/:id" exact component={SignUp} />
            <Route path="/panel" exact component={Panel} />
            <Route path="/about" exact component={About} />
            <Route path="/users/:id" exact component={UserInfo} />
            <Route path="/users/:id/products/:mode" exact component={Actions} />
            <Route path="/forgot" exact component={Forgot} />
            <Route path="/products/:id" exact component={AddProduct} />
            <Route path="/chats/:id" exact component={Chat} />
            <Route path="/items/:id" exact component={ProductView} />
          </Switch>
          <Footer/>
        </div>
      </Router>
    )
  }
}

export default App
