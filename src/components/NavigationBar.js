import React from "react"
import { Nav, Navbar, NavDropdown} from "react-bootstrap"
import Logo from "../img/logo.png"
import { Link } from "react-browser-router"

const myID = localStorage.getItem('userId'), myRole = localStorage.getItem('userRole')
const roleMap = {
    "ROLE_ADMIN" : {logo: "/panel", options: [{path: "/panel", setting: "Panel"}]},
    "ROLE_CLIENT" : {
      logo: "/home",
      options: [{path: "/join/" + myID, setting: "Edit Profile"},
      {path: "/users/" + myID + "/products/offer", setting: "My Offers"}, 
      {path: "/users/" + myID + "/products/purchase", setting: "Purchases"}, 
      {path: "/users/" + myID + "/products/bid", setting: "Watch list"}] 
    }
}
const role = roleMap[myRole]

// help user navigate throughout e-bid using a navigation bar 
function NavigationBar() {

  // display 'Log out' and link parent directory(HomePage) in case the user is already logged in and wishes to exit
  //if the token exists in session storage then the user is logged in 
  const logOut = !myID ? null :
    <NavDropdown title = "Settings" id = "basic-nav-dropdown">
        {role && role.options.map((option, index) =>  
          <NavDropdown.Item key={index} href={option.path}>
          {option.setting}
          </NavDropdown.Item>)}
        <NavDropdown.Divider />
        <NavDropdown.Item href="/" onClick={() => localStorage.clear()}>Log Out</NavDropdown.Item>
    </NavDropdown>

  return (
    <Nav className="navbar navbar-expand-md navbar-dark bg-dark">

      {/* display the App and e-bid logo, which both on click lead to homepage */}
      <Navbar.Brand className="mr-auto" style={{marginLeft : "5%"}}>
        <Link to={myID ? role.logo : "/"}><img className="img-responsive linkNav" src={Logo} height="40" alt="logo" /></Link>
      </Navbar.Brand>

      {/*help user navigate throughout e-bid*/}
      <Navbar>

        {/* Go to chat - messenger */
        myRole === "ROLE_CLIENT" && 
          <Navbar>
            <Link to="/chats/0" id="chat" className="linkNav">
                <i className="fab fa-facebook-messenger"></i>
            </Link>
          </Navbar>
        }

        {/* Advanced Search form link*/}
        {myRole === "ROLE_ADMIN" ||
        <Navbar>
          <Link to="/search" id="search" className="linkNav">
              Search <i className="fas fa-search-plus"></i>
          </Link>
        </Navbar>}

        {/*display 'Sign in' and onClick -> link the Login Page in case the user is not logged in yet*/}
        {myID === null && 
        <Navbar>
          <Link to="/login" className="linkNav">
            Sign in <i className="fa fa-user"></i>
          </Link>
        </Navbar>}

        {/* Sign up form link*/}
        {myID === null && 
        <Navbar>
          <Link to="/join/0" id="sign-up">
              Sign up <i className="fa fa-user-plus"></i>
          </Link>
        </Navbar>}

        {/* display 'Log out' and link HomePage in case the user is already logged in and wishes to exit*/}
        {logOut}
        
      </Navbar>
    </Nav>
  )
}

export default NavigationBar