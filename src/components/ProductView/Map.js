import React from 'react'
import { withGoogleMap, GoogleMap, Marker } from "react-google-maps"

class Map extends React.Component{
    constructor(props){
        super(props)
        this.state = { location: this.props.location }
    }

    render() {
      // get map and marker
      const MapWithAMarker = withGoogleMap(props => 
        <GoogleMap 
            defaultZoom={8} 
            defaultCenter={{ lat: this.state.location.lat, lng: this.state.location.lng }} >
            <Marker position={{ lat: this.state.location.lat, lng: this.state.location.lng }} />
        </GoogleMap>)

      return (
        <div>
            <MapWithAMarker 
              containerElement={<div style={{ height: "300px" }} />} 
              mapElement={<div style={{ height: "100%" }} />} 
            />
        </div>
      )
    }
}

export default Map