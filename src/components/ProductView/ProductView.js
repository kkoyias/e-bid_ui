import React from "react"
import ReactDOM from "react-dom"
import Rating from "react-rating"

import Product from "./rsrc/product_view_testing.json"
import ImageSlider from "./ImageSlider"
import { Load } from '../Utils/Utils'
import AlertBox from '../Utils/AlertBox/AlertBox'
import { Link } from "react-browser-router"
//import Map from "./Map"
import "./rsrc/product.css"

const maxRating = 10

class ProductView extends React.Component {
  constructor(props) {
    super(props)
    this.state = {id : this.props.match.params.id, clientID: parseInt(localStorage.getItem('userId'))}
    this.onSubmit = this.onSubmit.bind(this)
    this.fetchItem = this.fetchItem.bind(this)
  }

  componentDidMount() {
    ReactDOM.findDOMNode(this).addEventListener('click', () => this.setState({alerts: []}))
  
    this.fetchItem()
    
    // mark item as viewed from this user if not a guest
    if(this.state.clientID){
      fetch("http://localhost:8080/api/clients/" + this.state.clientID + "/views/" + this.state.id, {
        method: "POST",
        headers:{
          "Content-Type": "application/json"
        }
      })
      .then(httpResponse => console.log(httpResponse))
      .catch(error => console.error("Mark as viewed:", error))
    }
  }

  fetchItem(){
    const identification = this.state.clientID ? {"Identification": this.state.clientID} : {}

    // fetch product info
    fetch("http://localhost:8080/api/items/" + this.state.id, {
      method: "GET",
      headers: {...identification, "Content-Type": "application/json"}
        
      }).then(response => response.json())
      .then(resJSON => {
        this.setState({
          product: resJSON,
          isLoaded: true,
          rating: resJSON.userRating,
          expired: new Date(resJSON.endsAt) < new Date() || resJSON.sold
        })
      })
      .catch(error => {
        console.error("Get product:", error)
        this.setState({alerts: [{message: "Lost connection. Please try again later", type: "danger"}]})
      })
  }

  // make a bid or rate an item
  onSubmit(event, isBid) {
    event.preventDefault()
    const {id, clientID, bid, product, rating} = this.state, buy = bid && (bid.amount >= product.buyPrice)

    // fetch product info
    fetch("http://localhost:8080/api/items/" + id + '/' + (isBid ? "bid" : "rating") + "s/" + clientID, {
      method: "POST",
      headers:{
        //"Authorization": "Bearer " + localStorage.getItem("token"),
        "Content-Type": "application/json"
      },
      body: JSON.stringify({...(isBid ? bid : {rating: rating}), timeStamp: new Date()})
    })
    .then(response => {
      console.log(response)
      if(response.status === 201){
        if(isBid)
          window.location = buy ? "/chats/" + product.seller.id : "/users/" + clientID + "/products/bid"
        else{
          this.setState({alerts: [{message: "Your rating has been submitted", type: "success"}]})
          this.fetchItem()
        }
      }
    })
    .catch(error => {
      this.setState({alerts: [{
        message: "Something went wrong. Please check your connection & try again",
        type: "danger"
      }]})
      console.error("onSubmit error:", error)
    })
  }

  render() {

    // get all images
    const {product, expired, clientID, id, isLoaded, rating} = this.state, seller = product && product.seller
    const isInvalid = isLoaded && (expired || isNaN(clientID) || (seller && clientID === seller.id))
    console.log(this.state)

    return (
        <div className="container py-5">
          <div className="row">
            <div className="col-10 mx-auto col-md-6 my-3">
              <ImageSlider images={Product.gallery}/>
            </div>
            {
              this.state.isLoaded ? 
              <div className="col-10 mx-auto col-md-6 my-3 product-info">
                <h4 className="text-uppercase">
                  <b>{product.name}</b>
                </h4>
                {product && seller &&
                <h6>
                  By: <span className="font-normal">{seller.userName} [rating: {seller.ratingSeller}]</span>
                </h6>}
                <h6>Best offer: <span className="font-normal">{product.current}$</span></h6>
                {clientID ? 
                  <h6>
                    Rating: <span className="font-normal">{product.rating && product.rating.toFixed(2)}</span>
                  </h6> : null}
                <span className="text-danger">Ends: {product.endsAt}</span>
                <h2 className="text-warning">${product.buyPrice}</h2>

                {!isInvalid &&
                <form className="input-group" onSubmit={(e) => this.onSubmit(e, true)}>
                    <input 
                      type="number" 
                      className="form-control" 
                      name="bid" 
                      step={0.01}
                      placeholder={product.bid ? "Last bid: " + product.bid + '$': "Make a bid..."} 
                      required
                      min={(product.current && product.current.toFixed(2)) + 0.01}
                      max={product.buyPrice && product.buyPrice.toFixed(2)}
                      onChange = {(event) => this.setState({bid : {amount : event.target.value}})}
                    />
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>}
                <dl> <br/>
                  {/*product.location && <Map location={product.location}/>*/}
                  <br/>
                  <dt>Description</dt>
                  <dd><p>{product.description}</p></dd>
                </dl>
                {seller && clientID === seller.id && <Link to={"/products/" + id}>Edit</Link>}
              </div>
              :
              <Load/>
            }
          </div>
          {!isInvalid &&
              <div id="product-rate">
                  <h6 className="display-4">
                    Rate product <i className="fa fa-paper-plane" onClick={(e) => this.onSubmit(e, false)}></i>
                  </h6>
                  <Rating 
                    stop={maxRating} 
                    onClick={(value) => this.setState({rating: value})}
                    initialRating={rating || 0}
                  />
          </div>}
          <div id="product-alert-box">
            {this.state.alerts && <AlertBox state={this.state} handler={() => this.setState({alerts: []})}/> }
          </div>
       </div>
    )
  }
}

export default ProductView