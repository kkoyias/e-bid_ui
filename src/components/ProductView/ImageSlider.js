import React from 'react'
import { Gallery, GalleryImage } from "react-gesture-gallery"

class ImageSlider extends React.Component {
    constructor(props) {
        super(props);
        this.state = { index: 0 };
    }

    render() {
        return (
            <Gallery index={this.state.index} onRequestChange={i => {this.setState({ index: i })}}>
                {this.props.images.map(image => ( <GalleryImage key={image} objectFit="contain" src={image} /> ))}
            </Gallery>
        );
    }
}

export default ImageSlider