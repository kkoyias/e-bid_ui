import React from "react"
import { Link } from "react-browser-router"
import UserImage from "./rsrc/user_profile_image.png"
import {authorizeUser} from "../Utils/Utils"
import {Redirect} from "react-router-dom"
import "./rsrc/admin.css"

class UserInfo extends React.Component {
  constructor(props) {
    super(props)
    
    this.state = {
        userInfo: {},
        userID: this.props.match.params.id,
        loading: true
    }
    this.onClick = this.onClick.bind(this)
  }

  componentDidMount() {

    // fetch the data => retrieve user info
    fetch("http://localhost:8080/api/clients/" + this.state.userID, {
      method: "GET",
      headers:{
        "Authorization": "Bearer " + localStorage.getItem("token"),
        "Content-Type": "application/json"
      }
      })
      .then(response => response.json())
          .then(resJSON => this.setState({
            userInfo: resJSON, 
            loading: false
          }))
      .catch(error => console.error("Retrieve User Error:", error))
  }

  onClick = event => {
    event.preventDefault()
    this.updateUserStatus(event.target.id)
  }

  updateUserStatus(status) {

    fetch("http://localhost:8080/api/clients/" + this.state.userID, {
      method: "PATCH",
      headers:{
        "Authorization": btoa("admin"),
        "Content-Type": "application/json"
      },
      body: JSON.stringify({status : status})
    })
    .then(response => response.json())
      .then(resJSON => window.location = "/panel")
    .catch(error => console.error(status + " User Error: ", error))
  }

  render() {

    // deny access to all users other than the administrator
    const location = authorizeUser("ROLE_ADMIN")
    if (location !== "")
        return <Redirect to={location} />

    // map each attribute of the client to field of this profile card
    let result = [], index = 0
    const {userInfo} = this.state

    // for each attribute of the user
    for(let key in userInfo){

      // if it is not a collection
      if(key !== "_links" && key !== "password"){
        index++

        // display it's value or a dash if not available
        const value = userInfo[key] || '-'
        result.push(
          <div key={index} className="row">
            <div className="user-field col-sm-3 col-md-2 col-5">
              <label><b>{key.toLowerCase()}</b></label>
            </div>
            <div className="col-md-8 col-6">
              {value}
            </div>
          </div>
        )
      }
    }

    //if the user status is pending show the buttons, otherwise do not
    const buttons = this.state.userInfo.status === "pending" &&
                      [<button className="btn btn-success m-1 p-2" id="active" onClick={this.onClick}>Approve</button>,
                      <button className="btn btn-danger m-1 p-2" id="rejected" onClick={this.onClick}>Reject</button>]

    return (
      !this.state.loading &&
      <div className="container"> <br/>
        <div className="card card-body col-12">
          <div className="image-container">
            <img className="img-responsive" src={UserImage} height="250" alt="logo" />
          </div> <br/>
          <div className="row">
            <div className="col-12">
              <div className="nav nav-tabs mb-4" id="myTab" role="tablist"> 
                <h5 className="nav-link active">About</h5>
              </div>
              <div className="tab-content ml-1" id="myTabContent">
                {result}
              </div>
            </div>
          </div>
        </div>
        <br/>
        <div align="center">
          {buttons}<br/><br/>
          <Link id="back-to-panel" to="/panel">Back to Panel</Link>
        </div>
      </div>
    )
  }
}

export default UserInfo