import React from "react"
import { Redirect, Link } from "react-router-dom"
import { authorizeUser } from "../Utils/Utils"
import { Col, Spinner} from 'react-bootstrap'
import Pagination from 'react-bootstrap/Pagination'

const itemsPerPage = 5, pageRange = 3

class Panel extends React.Component {
    constructor() {
        super()
        this.state = {
          userList: [],
          isLoaded: false,
          activePage: 1,
          lastPage: 1,
          pages: 1
        }
    }

    componentDidMount() {

        // fetch the data => retrieve all users
        fetch('http://localhost:8080/api/clients', {
            method: 'GET',
            headers:{
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Content-Type': 'application/json'
            }
        }).then(response => response.json())
        .then(resJSON => {
            this.setState({
                userList: resJSON,
                isLoaded: true,
                activePage: 1,
                lastPage : Math.ceil(resJSON.length/itemsPerPage),
                pages : this.getPages(1, Math.ceil(resJSON.length/itemsPerPage))
            })
        })
        .catch(error => console.error('Retrieve All Users Error:', error))
    }

    // given a page range return a list with Pagination Item/Ellipsis components
    getPages(active, last){
        let pages = [], start = (active >= 1 && active < pageRange) ? 1 : active
        if(start > 1)
            pages.push(<Pagination.Ellipsis key = {0}/>)
    
        for(let i = start; i < start + pageRange && i <= last; i++){
            pages.push(
                <Pagination.Item 
                    onClick = {(event) => this.onPageChange(parseInt(event.target.innerText))} 
                    key = {i} 
                    active = {i === active}>{i}
                </Pagination.Item>
            )
        }
    
        if(start + pageRange -1 < last)
            pages.push(<Pagination.Ellipsis key = {last + 1}/>)
        return pages
    }

    // if page requested is in bounds, update page slides and set active page appropriately 
    onPageChange(selected) {
        if(selected >= 1 && selected <= this.state.lastPage)
            this.setState({activePage: selected, pages : this.getPages(selected, this.state.lastPage)})
    }

    render() {

        // deny access to all users other than the administrator
        const location = authorizeUser("ROLE_ADMIN")
        if (location !== "")
            return <Redirect href={location} />

        const startIndex = (this.state.activePage - 1)* itemsPerPage
        const colorsByStatus = {
            pending : "orange", 
            rejected : "red", 
            active : "green", 
            undefined : "black", 
            banned : "purple"
        }
            

        const panel = this.state.userList instanceof Array && 
            this.state.userList.slice(startIndex, startIndex + itemsPerPage).map(item => {
        const status = <td style={{color: colorsByStatus[item.status]}} align="center">
                        {item.status ? item.status : "unavailable"}
                    </td>
            
        return (
            <tr key={item.userName}>
                <td align="center">{item.userName}</td>
                {status}
                <td align="center">
                    <Link to={"/users/" + item.id}>
                        <button id={item.id} type="button" className="btn btn-primary">
                            View Profile
                        </button>
                    </Link>
                </td>
            </tr>)
        })

        console.log(this.state)

        return (
        <div className="container-fluid" align="center">
            <h1 className="note note-right">
                {"Export items as "} 
                <a href={"http://localhost:8080/api/items"} download="items.json">JSON</a>
                    {" | "}
                <a href={"http://localhost:8080/api/items"} download="items.xml">XML</a>
            </h1>
            {this.state.isLoaded ?
            <div className="row col-md-12 custyle" align="center">
            
            <table className="table table-striped custab" align="center">
                <thead align="center">
                <tr align="center">  
                    <th>Username</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>{panel}</tbody>
            </table>
            <Col md={{ offset: 5 }}>
                <Pagination style = {{textAlign : "center"}}>
                    <Pagination.First onClick = {() => this.onPageChange(1)}/>
                    <Pagination.Prev onClick = {() => this.onPageChange(this.state.activePage - 1)}/>
                    {this.state.pages}
                    <Pagination.Next onClick = {() => this.onPageChange(this.state.activePage + 1)}/>
                    <Pagination.Last onClick = {() => this.onPageChange(this.state.lastPage)}/>
                </Pagination>
            </Col>
            </div> : <Spinner animation="border"/> 
            }



        </div>
        )
    }
}

export default Panel