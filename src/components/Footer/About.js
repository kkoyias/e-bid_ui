import React from "react"
import Members from "./rsrc/about.json" //get info for all members of this team
import { Container, Jumbotron, Row, Col } from "react-bootstrap"

// import images for each member of the team
import Koyias from "./rsrc/koyias.jpeg"
import Alex from "./rsrc/alex.png"

// This page displays some information about the purpose and the members of the development team
function About() {
  // store all imported images in a list
  const imgs = [Koyias, Alex]

  // transform each member record to a column containing a form and store all these columns in an array
  const cols = Members.map((member, index) => (
    <Col key={member.sdi} >
      <form style={{float : !(index % 2) ? "right" : "left", fontFamily : "arial"}}>
        <fieldset>
          <h6>{member.name} </h6>
          <img className="profile" src={imgs[index]} alt={member.name} />
          <br />
          <strong>E-mail</strong>:{" "}
          <a href={"mailto:" + member.sdi + "@di.uoa.gr"}>
            {member.sdi}@di.uoa.gr
          </a>
          <br />
          <strong>Github</strong>:{" "}
          <a href={member.git} target="_blank" rel="noopener noreferrer">
            {member.git}
          </a>
        </fieldset>
      </form>
    </Col>
  ))

  // the following will be displayed on the screen, a quick description of what we do and who we are
  return (
    <Jumbotron id="about-container">
      <h1>About e-bid</h1>
      <p style={{ textAlign: "center", fontFamily : "arial" }}>
        Invented by a group of undergraduate students in UoA,<br/>e-bid is on a
        mission of making online bidding as simple as possible.
      </p>
      <h1>Meet the Team</h1>
      <Container>
        <Row>
          {cols}
        </Row>
      </Container>
    </Jumbotron>
  )
}

export default About
