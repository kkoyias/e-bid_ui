import React from "react"
import { Container, Col, Row } from "react-bootstrap"
import {Link} from "react-browser-router"
import info from "./rsrc/footer.json"

// This is the bottom section of the webpage. It contains the name of the company along with relevant copyright information
// some basic navigation links and any other extras the team might come up with (quotes etc.)
function Footer() {
  const subFooter = info.map((item, index) => {
    return (
      <Col key ={index} sm={item.position}>
        <h4 className="footer-title">{item.label}</h4>
        {item.image && React.createElement("img", item.image)} 
        {item.content && item.content.map((content, index) => {
          return (
            <div key={index}>
              <Link to={content.href || "/"} className="footer-link">
                {content.field}
              </Link>
            </div>
          )
        })}
      </Col>
    )
  })

  return (
    <Container fluid={true} className="footer">
      {/* Sub-footer */}
      <Row className="footer-subfooter">
        {subFooter}
      </Row>
      {/* Footer */}
      <Row className="footer-footer bg-dark">
        <Col>
          <p className="text-light">{"Designed & Developed by Project Inc."}</p>
        </Col>
      </Row>
    </Container>
  )
}

export default Footer