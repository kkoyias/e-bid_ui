import React from "react"
import ReactDOM from "react-dom"

import { Button, Form} from "react-bootstrap"
import { Link, Redirect } from "react-browser-router"
import InputIcon from "../Utils/InputIcon"
import {authorizeUser} from "../Utils/Utils"
import fields from "./rsrc/sign-in"
import "./guest.css"

const locationMap = {ROLE_ADMIN: "/panel", ROLE_CLIENT: "/home"}

// this component displays a login form, reviews it on submission and checks user in, if submission is valid
class SignIn extends React.Component {
  constructor() {
    super()
    this.state = {errors: []}

    this.onChange = this.onChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  // on mounting fetch trending products to display
  componentDidMount(){
    ReactDOM.findDOMNode(this).addEventListener('click', () => this.setState({errors: []}))
  }

  // update username or password on each keystroke
  onChange = event => {
    const {name, value} = event.target
    this.setState({[name] : value })
  }

  // after submission is completed, review it
  onSubmit(event) {
    event.preventDefault()
    fetch('http://localhost:8080/api/clients/login', {
      method: 'GET',
      headers: new Headers({
        'Authorization': btoa(this.state.identifier + ":" + this.state.password),
        "Content-Type": "application/json"
      })
    })
      //response-return json
      .then(httpResponse => {
        if(httpResponse.status === 401)
          this.setState({errors : ["Incorrect username or password!"]})
        else if(httpResponse.status === 426)
          this.setState({errors: ["Your registration is pending approval from the administrator"]})
        else 
          return httpResponse.json()
      })
      .then((stringResponse) => {
        const jsonResponse = JSON.parse(stringResponse)

        localStorage.setItem('token', jsonResponse.accessToken)
        localStorage.setItem('userId', jsonResponse.userId)
        localStorage.setItem('userRole', jsonResponse.userRole)
        window.location = locationMap[jsonResponse.userRole];
      })

      // display an error message in case of being unable to connect
      .catch(error => {
        console.error("Error, login: " + error)
        if(this.state.errors.length === 0)
          this.setState({errors: ["Failed to establish connection. Please try again later."]})
      })
  }

  // convert a JSON object to an input HTML element
  getInput(item){
    const commonFields = {
      required : true,
      onChange : this.onChange,
      className: "form-control"
    }

    return (
      <Form.Group key={item.label}>
        <Form.Label><strong>{item.label}</strong></Form.Label>
        <br />
        <div className="input-group">
          <InputIcon icon={item.icon}/>
          {React.createElement("input", {...item.input, value : (this.state[item.input.name] || ""),...commonFields})}
        </div>
      </Form.Group>
    )
  }

  // render a log in form with a log in button
  render() {
    const location = authorizeUser("GUEST")
    if(location !== "")
      return <Redirect to={location} />

    const errors = this.state.errors
      .map((error, index) => <div key={index} className="alert alert-danger alert-dismissible">{error}</div>)
    console.log(this.state)

    return (
      <Form className = "guestForm" onSubmit={this.onSubmit}>
        <legend id="sign-in">Sign in to e-bid</legend>
        <fieldset className="guestFieldSet">
          {errors}
          <img 
            src="https://cdn.pixabay.com/photo/2017/07/18/23/23/user-2517433__340.png" 
            height="80" width="90" id="login-img" alt="user-icon"
          />
          {fields.map(this.getInput.bind(this))}<hr/>
          <Button type="submit" variant="primary">Sign in</Button>
        </fieldset>
        <br/><br/>
        <a href="/forgot" target="_blank"> Forgot password?</a>
        <br />
        New to e-bid? <Link to="/join">Sign up</Link>
      </Form>
    )
  }
}

export default SignIn