import React from "react"
import { Button } from "react-bootstrap"
import { Link } from "react-browser-router"
import messages from "./rsrc/message"

function Reset(props){

    return (
            <form className="guestForm" onSubmit={props.onSubmit}><br/>
                <img 
                    src="https://cdn.onlinewebfonts.com/svg/img_398183.png" 
                    style={{height : "70px", width : "70px" }}
                    alt="reset-icon"/><br/><br/>
                <label style={{fontSize : "x-large"}}>Reset your password</label><br/>

                <fieldset className="guestFieldSet">
                    <p>Enter your email address and we will send<br/> you a link in order to reset your password.</p>
                    <input
                        type = "email" 
                        placeholder="Enter your email address" 
                        autoFocus
                        required
                        value = {props.value}
                        onChange = {props.handler}
                    /><hr/>

                    <Button variant="success" type="submit">
                        Send password reset email
                    </Button><br/>

                </fieldset><br/><br/>
            </form>)
}

function Message(props){
    const {link, img, location, paragraph} = messages[props.type]

    return(
        <div className="guestForm"><br/>
            <img 
                src={img.url}
                style={img.style}
                alt={props.type + "icon"}/><br/><br/>
            <fieldset  className="guestFieldSet">
                <p style={{textAlign: "center"}}>
                    {paragraph}
                </p><br/>
                <Link to={link}><Button variant="outline-dark">Return to {location}</Button></Link>
            </fieldset><br/><br/>
        </div>
    )
}

export {Reset, Message}