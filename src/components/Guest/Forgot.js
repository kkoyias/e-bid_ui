import React from 'react'
import {Reset, Message} from "./ResetFunctions"

class Forgot extends React.Component{
    constructor(){
        super()
        this.state = {submit : false}
        this.onSubmit = this.onSubmit.bind(this)
        this.onChange = this.onChange.bind(this)
    }

    // on submit send email to the user
    onSubmit(){
        console.log(this.state.email || "no email")
        this.setState({submit : true})
    }

    onChange = (event) => this.setState({email : event.target.value})

    render(){
        console.log(this.state)
        if(this.state.submit)
            return <Message type="return"/> 
        else
            return (
            <Reset 
                onSubmit={this.onSubmit} 
                value={this.state.email || ""}
                handler={this.onChange} 
            />)
    }

}

export default Forgot