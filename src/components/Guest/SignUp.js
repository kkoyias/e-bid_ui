import React from 'react'
import ReactDOM from "react-dom"

import { Button, Container} from "react-bootstrap"
import { Link, Redirect } from "react-browser-router"
import {InputGenerator, authorizeUser} from "../Utils/Utils"
import fields from "./rsrc/sign-up"

// This component displays a sign-up form, reviews it on submission and checks user in, if submission is valid
class Signup extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: "",
      errors: [],
      id : this.props.match.params.id
    }
    this.onSubmit = this.onSubmit.bind(this)
    this.responseHandler = this.responseHandler.bind(this)
    this.deleteAccount = this.deleteAccount.bind(this)
  }

  componentDidMount(){
    ReactDOM.findDOMNode(this).addEventListener('click', () => this.setState({errors: []}))

    const {id} = this.state
    if(id > 0){
      fetch("http://localhost:8080/api/clients/" + id)
      .then(httpResponse => httpResponse.json())
        .then(({password, ...data}) => this.setState({...data, name: data.userName}))
      .catch(error => console.error(error))
    }
  }

  // handle the backend response on signup
  responseHandler = (httpResponse) => {

    // in case of a successful signup redirect to the pending page
    if(httpResponse.status >= 200 && httpResponse.status < 300){ 
      this.state.id > 0 && localStorage.clear()
      window.location = "/pending"
    }

    // else if the username exists, prompt user to pick another
    else if(httpResponse.status === 409)
      this.setState({errors : ["Username exists"]})
    else 
      console.error("sign-up: received response " + httpResponse.status);
  }

  // review submission 
  onSubmit = event => {
    event.preventDefault()
    const {confirm, exists, id, name, password, retype, ...payload} = this.state
    payload.status = "pending"
    console.log(JSON.stringify(payload, id))

    // if passwords match then ask for the user to be registered
    if(password === retype) {
      fetch("http://localhost:8080/api/clients" + (id > 0 ? "/" + id : ""), {
        method: id > 0 ? "PATCH" : "POST",
        headers: new Headers({
          'Authorization': btoa(this.state.password),
          "Content-Type": "application/json"
        }),
        body: JSON.stringify(payload)
      })
      // redirect to Pending page after response
      .then(this.responseHandler)

      //reject-error
      .catch((error) => {console.error("Error: ", error);this.setState({errors: ["Please try again later"]})})
    } 
    else {
      this.setState({errors : ["Passwords do not match"]})
      console.log("Passwords do not match: " + password, retype)
    }
  }

  deleteAccount(){
    let confirm = window.confirm("Want to permanently delete your account")
    if(confirm){
      fetch("http://localhost:8080/api/clients/" + this.state.id, {
        method: "DELETE",
      })
      // redirect to Welcome page after response
      .then(httpResponse => {
        if(httpResponse.status === 405) // method not allowed
          this.setState({errors: ["Sorry, you have active offers. Please cancel all of them before deleting this account"]})
        else{
          localStorage.clear()
          window.location = "/"
        }
      })

      //reject-error
      .catch(error => {console.error("Error: ", error);this.setState({errors: ["Please try again later"]})})
    }
  }

  // display a signup form on the screen
  render() {

    // redirect logged in users appropriately
    const location = this.state.id > 0 ? authorizeUser("ROLE_CLIENT", this.state.id) : authorizeUser("GUEST")
    if(location !== "")
      return <Redirect to={location}/>


    const errors = this.state.errors.map((error, index) => 
          <div key={index} className="alert alert-danger mx-auto my-5px ">
            {error} 
            <i className="fa fa-times" onClick={() => this.setState({errors: []})}></i> 
          </div>
    )

    const editMode = parseInt(this.state.id) > 0, legend = editMode ? "Hey " + this.state.name : "Join e-bid today for Free"

    return (
      <Container className="guestForm" style={{backgroundColor : "white" }}>
        <form onSubmit={this.onSubmit}>
          <legend style={{fontFamily : "B612", fontSize : "2.0rem"}}>{legend}</legend><br/>
          <InputGenerator id={this} list={fields}/>

          {errors}

          <hr/>
          <Button type="submit" variant="primary">
            {editMode ? "Commit changes" : "Create an account"}
          </Button>
          {editMode && <Button variant="danger" id="delete" onClick={this.deleteAccount}>Delete account</Button>}
          {!editMode && <span><br/>Already have an account? <Link to="/login">Log in</Link></span>}
        </form>
      </Container>
    )
  }
}

export default Signup
