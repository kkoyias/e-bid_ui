import React from 'react'
import {Load, authorizeUser} from "../../Utils/Utils"
import Listing from "../../Results/Listing"
import {config, modeMap } from "./Configs"
import {Link, Redirect} from "react-browser-router"
import "./rsrc/actions.css" 

class Actions extends React.Component{
    constructor(props){
        super(props)
        this.state = {id: this.props.match.params.id, loading: true, mode: this.props.match.params.mode, present: true}
    }

    // on mounting fetch trending product images to display
    componentDidMount(){
        const map = modeMap[this.state.mode], url = map.url(this.state.id), formatProducts = map.formatProducts
        console.log("API call to: " + url)

        fetch(url)
            .then(response => response.json())
            .then(data => this.setState({
                loading: false, 
                products : formatProducts ? formatProducts(data) : data
            }))
            .catch(error => console.error("Error on welcome fetch: ", error))
    }

    render(){
        // make sure this is the right client
        const location = authorizeUser("ROLE_CLIENT", this.state.id)
        if(location !== "")
            return <Redirect to={location}/>

        const {mode, present, products, loading} = this.state, iconInfo = modeMap[mode].icon
        const icon = iconInfo && <Link to={iconInfo.location}><i className={iconInfo.className}></i></Link>
        console.log(this.state)

        return(
            <div align="center">
                <h1 
                    id="my-prod">{modeMap[mode].legend}
                    {icon}
                </h1>
                {mode !== "purchase" && 
                <button
                    id="actions-filter" 
                    onClick = {() => this.setState({present : !present})}
                    className="btn btn-outline-info">
                    {"View " + (present ? "Expired" : "Active")}
                </button>}
                {loading ?  <Load/> : 
                    <Listing 
                        mode={modeMap[mode].action(present)} 

                        // display either -expired or sold- products or -active-
                        list={products.filter(modeMap[mode].filterFunction(present))} 
                        config={config}
                    />}
            </div>
        )
    }

}

export default Actions