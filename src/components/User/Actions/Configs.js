const config = {prodsPerRow : 3, useRows : 2}
const getProd = ((item, index) => {
    return {
        id : item["_links"].self.href.slice(item["_links"].self.href.lastIndexOf("/") + 1), 
        name : item.name.slice(0,1).toUpperCase() + item.name.slice(1), 
        endsAt: item.endsAt,
        location : "Athens", 
        description : item.description,
        sold: item.sold
    }
})

function extractLocation(item){
    const rv = item
    rv.location = item.location ? item.location.address : "Unknown location"
    return rv
}

const modeMap = {
    offer: {
        legend: "My Offers",
        url: (id => "http://localhost:8080/api/clients/" + id + "/items"),
        formatProducts: (data => data["_embedded"].items.map(getProd)),
        action: (present => present ? "edit" : "check"),
        icon:{
            location: "/products/0",
            className: "fas fa-plus-circle"
        },
        filterFunction: (present) => filterItems[present ? "active" : "expired"]
    },
    bid:{
        legend: "My Watch list",
        url: (id => "http://localhost:8080/api/clients/" + id + "/bids"),
        formatProducts: (data => data.map(extractLocation)),
        action: () => "check",
        filterFunction: (present) => {
            return filterItems[present ? "active" : "expired"]
        }
    },
    purchase : {
        legend: "My Purchases",
        url: (id => "http://localhost:8080/api/clients/" + id + "/purchases"),
        formatProducts: (data => data["_embedded"].items.map(getProd)),
        action: () => "check",
        filterFunction: (present) => filterItems["expired"]
    }
}

const filterItems = {
    expired: (item => new Date() > new Date(item.endsAt) || item.sold),
    active: (item => new Date() < new Date(item.endsAt) && !item.sold)
}

export {config, modeMap, filterItems}