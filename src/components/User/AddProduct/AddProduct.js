import React from 'react'
import Logo from './rsrc/logo.svg'
import formInfo from "./rsrc/product.json"
import AutoCompleteSelection from "./AutoCompleteSelection"
import {InputGenerator, authorizeUser} from "../../Utils/Utils"
import {Redirect} from "react-browser-router"
//import ImageUploader from 'react-images-upload'
//import AutoSuggestMaps from "./AutoSuggestMaps"

const toCurrency = (str) => parseFloat(str.slice(1).replace(",", "")), clientID = parseInt(localStorage.getItem('userId'))

class AddProduct extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      id: this.props.match.params.id, 
      location: {address: "", lat: 37.983810, lng: 23.727539 },
      errors : [], categories: [], description: ""}
    this.onSubmit = this.onSubmit.bind(this)
    this.update = this.update.bind(this)
    this.locationHandler = this.locationHandler.bind(this)
    this.deleteItem = this.deleteItem.bind(this)
  }

  componentDidMount(){
      const id = this.state.id
      if(id > 0){
        fetch("http://localhost:8080/api/items/" + id,{
          headers: {
            'Identification': clientID          
          }
        })
        .then(response => response.json())
            .then(data => this.setState(data))
        .catch(error => console.error("Error on getting a product: ", error))
      }
  }

  // upload product
  onSubmit = event => {
    event.preventDefault()
    const {id, errors, categories, image, ...item} = this.state, payload = {item, categories: categories.map(c => c.id)}
    const fetchInfo = {
      add: {url: "items/" + clientID, method: "POST"}, 
      edit: {url: "items/" + id, method: "PATCH"}
    }
    const info = id > 0 ? fetchInfo.edit : fetchInfo.add
    
    payload.item.firstBid = toCurrency(this.state.firstBid) 
    payload.item.buyPrice = toCurrency(this.state.buyPrice)
    console.log(fetchInfo, payload)

    // if all fields have taken valid and within the appropriate range value, add or edit this product 
    if(this.validateResult(payload.item)){
      fetch("http://localhost:8080/api/" + info.url + (image ? "?file=" + image : ""), {
        method: info.method,
        headers: new Headers({
          "Content-Type": "application/json"
        }),
        body: JSON.stringify(payload)
      })
      // display response
      .then(response => {
          console.log(response)
          if(parseInt(id) === 0)
            window.location = "/users/" + clientID + "/products/offer"
          else if(this.notAlterable(response) === false)
            window.location = "/items/" + this.state.id
        }
      )

      //reject-error
      .catch(error => {console.error(error); this.setState({errors: ["Please try again later"]})})
    }
  }

  // check the input against some conditions
  validateResult(result)
  {
    let errors = []
    const ends = new Date(result.endsAt), starts = new Date(result.startsAt), present = new Date()
    console.log(starts, ends, present)

    if(result.firstBid > result.buyPrice)
      errors = ["First Bid can not be larger than Buy Price"]

    // if this is an edit the start-time must not be set later than end-time
    // and end-time must not be set earlier than present
    if(this.state.id && (starts > ends || present > ends))
      errors.push(["End time must be later than both Start & Present times"])

    // if this is a new product, start-time shall be later than present
    // and end-time later than start-time
    if(this.state.id && (starts < present || starts > ends ))
      errors.push(["Start time must be later than Present time"])

    this.setState({errors})
    return errors.length === 0
  }

  notAlterable(response){
    if(response.status === 405){
      this.setState({errors : ["Item is not alterable after either a bid has been made or the auction has started"]})
      return true
    }
    return false
  }

  deleteItem(){
    const confirm = window.confirm("Are you sure of deleting this offer?");
    if(confirm){
      fetch("http://localhost:8080/api/items/" + this.state.id, {
        method: "DELETE",
      })
      // redirect to My Products page after response
      .then(httpResponse => {
        console.log(httpResponse)
        if(this.notAlterable(httpResponse) === false)
          window.location = "/users/" + clientID + "/products/offer"
      })

      //reject-error
      .catch(error => {console.error(error); this.setState({errors: ["Please try again later"]})})
    }
  }

  // update field categories from autocomplete selection list
  update(field, value) {

    // if list is empty, initialize it
    if(this.state[field] === undefined)
      this.setState({[field] : [value]})

    // else if list contains this item already, remove it 
    else if (this.state[field].map(item => item.id).includes(value.id))
      this.setState({[field] : this.state[field].filter((item) => item.id !== value.id)})

    // else add this item to the list
    else 
      this.setState({[field] : [...this.state[field], value]})

  }

  // update location with { lat, lng }
  locationHandler(location) {
    this.setState({location : location})
  }

  render() {

    // user can only edit his own item
    const location = authorizeUser("ROLE_CLIENT")
    if(location !== "")
      return <Redirect to={location}/>

    if(this.state.id > 0 && this.state.seller && this.state.seller.id !== clientID)
      return <Redirect to="/home"/>

    // create the container layer
    const layer = <div className="col-md-4 py-5 bg-dark text-white text-center">
                    <img className="img-responsive" src={Logo} alt="logo" />
                    <h2 className="py-3">{formInfo.header}</h2>
                    <h6>{formInfo.details}</h6>
                  </div>

    // create autocomplete categories and description                                                    
    const categories = <div className="form-group col-md-8 p-0 py-3" style={{zIndex: 1}}>
                          <b className="ml-1">Categories</b>
                          <AutoCompleteSelection 
                            update={this.update} 
                            categories={this.state.categories || []}/>
                       </div>

    const textArea = <div id="description" className="form-group col-md-12 p-0">
                        <b className="ml-1">Description</b>
                        <textarea 
                          className="form-control" 
                          placeholder="Please enter description..." 
                          name="description" 
                          form="product-form"
                          value={this.state.description}
                          onChange={(event) => this.setState({description : event.target.value}) }>
                        </textarea>
                      </div>

    // upload product button
    const button =  <div className="py-3" align="center">
                      <button type="submit" className="btn btn-outline-dark">
                        {this.state.id > 0 ? "Edit" : "Add"} Product
                      </button>
                      {this.state.id > 0 &&
                        <button id="delete-item" className="btn btn-outline-danger" onClick={this.deleteItem}>
                          Cancel auction
                        </button>
                      }
                    </div>

    const errors = this.state.errors.map((error, index) => <div key={index} className="product-errors">{error}</div>)
    const fields =  <form onSubmit={this.onSubmit} id="product-form">
                      <InputGenerator id={this} list={formInfo.fields}/>
                      {errors}
                      {/*<AutoSuggestMaps locationHandler={this.locationHandler} location={this.state.location}/>*/}
                      {categories}
                      {textArea}
                      {/*<ImageUploader
                        fileContainerStyle = {{padding : "0px", margin: "0px", backgroundColor : "#f0f0f0"}}
                        withIcon={true}
                        buttonText='Upload an image'
                        label = " Maximum size: 5MB "
                        labelStyles = {{border : '1px solid black', borderRadius : '6px', padding: "2px 16px", backgroundColor : "snow"}}
                        labelClass = "label label-info"
                        onChange={(files) => this.setState({image: files.map(file => file.name)[0]})}
                        imgExtension={['.jpg', '.png', '.gif']}
                        singleImage={true}
                        maxFileSize={5242880}
                      />*/}
                      <hr/>
                      {button}
                    </form>  

    const form =  <div className="row">
                    {layer}
                    <div className="col-md-8 py-5 border add-prod">
                      {fields}
                    </div>
                  </div>

    console.log(this.state)
        
    return (
        <div className="container py-5">
          {form}
        </div>
    )
  }
}

export default AddProduct