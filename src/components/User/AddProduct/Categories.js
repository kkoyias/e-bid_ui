// fetch all categories and apply a thenFunction to the corresponding json response
function getCategories(thenFunction){
    fetch("http://localhost:8080/api/categories")
        .then(response => response.json())
            .then(categories => thenFunction(categories))
        .catch(error => console.error(error))
}

// flatten the category hierarchy tree using DFS, returning a single level list with all categories
function getPlainCategories(categories){
    let rv = []
    for(let i = 0; i < categories.length; i++)
        rv.push(categories[i], ...getPlainCategories(categories[i].subCategories))
    return rv;
}

// fetch all categories, flatten them and set the corresponding list as the state.categories of appropriate component
function getCategoryList(id){
    getCategories(categories => id.setState({categories: getPlainCategories(categories)}))
}

// fetch all categories, and set them as the state.categories of the appropriate component 
function getCategoryHierarchy(id){
    getCategories(categories => id.setState({categories}))
}

export {getCategoryList, getCategoryHierarchy}