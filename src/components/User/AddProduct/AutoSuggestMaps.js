import React from 'react'
import Geosuggest from "react-geosuggest"
import { withGoogleMap, GoogleMap, Marker } from "react-google-maps"
import Geocode from "react-geocode"
import "./rsrc/addProd.css"

class AutoSuggestMaps extends React.Component{
    constructor(props){
        super(props)
        // initialize location to Athens
        this.state = { marker_flag: true}
        this.clickOnMarker = this.clickOnMarker.bind(this)
    }

    // fix marker when you click on a suggestion
    onSuggestSelect = (place) => {
      console.log(place)
      if(place !== undefined) {
        this.setState({ marker_flag: true})
        this.props.locationHandler(place.location)
      }
    }

    // fix marker when you click on the map and input location, also pass lat and lng to parent/AddProduct.js
    clickOnMap(event) {
      let locationClicked = { lat: event.latLng.lat(), lng: event.latLng.lng() }
      this.setState({ marker_flag: true })
      this.props.locationHandler(locationClicked)
      Geocode.setApiKey("AIzaSyCaEdcNK1ExBGTaX0ft1VyMB8GwLP85Y0Q")
      Geocode.fromLatLng(locationClicked.lat, locationClicked.lng).then(
        response => {
          const address = response.results[0].formatted_address
          this._geoSuggest.update(address)
        },
        error => {
          console.error(error)
          this._geoSuggest.update("error..")
        }
      )
    }

    // remove marker when you click on it
    clickOnMarker() {
      this.setState({ marker_flag: false })
    }

    render() {
      // get map and marker
      const MapWithAMarker = withGoogleMap(props => 
        <GoogleMap 
          defaultZoom={8} 
          defaultCenter={{ lat: this.props.location.lat, lng: this.props.location.lng }} 
          onClick={(event) => this.clickOnMap(event)} 
          >
          {this.state.marker_flag && 
          <Marker 
            position={{ lat: this.props.location.lat, lng: this.props.location.lng }} 
            onClick={this.clickOnMarker}
          />}
        </GoogleMap>)

      return (
        <div>
          <b className="ml-1">Address</b>
          <Geosuggest 
            ref={el=>this._geoSuggest=el} 
            placeholder="Search your location" 
            onSuggestSelect={this.onSuggestSelect} 
            location={new window.google.maps.LatLng(null)} 
            radius={20} 
            required
          />
          <MapWithAMarker 
            containerElement={<div style={{ height: "400px" }} />} 
            mapElement={<div style={{ height: "100%" }} />} 
          />
        </div>
      )
    }
}

export default AutoSuggestMaps