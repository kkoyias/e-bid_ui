import React from 'react'
import "./rsrc/addProd.css"
import RemoveLogo from "./rsrc/remove_x_logo.png"
import {getCategoryList} from "./Categories"

class AutoCompleteSelection extends React.Component {
  constructor(props) {
    super(props)
    this.state = {categories: [], suggestions: [], input_text: "", selected: props.categories}
  }
  
  componentDidMount() {
    console.log(this.state)
    getCategoryList(this)
  }

  // update categories for this product after parent component fetches them with an API call
  componentWillReceiveProps(props, state){
    if(props.categories !== state.selected)
      this.setState({selected: props.categories})
  }

  // while typing
  onInputChange = (e) => {
    const input = e.target.value

    let suggestions = []
    if(input.length > 0) {
      const regExp = new RegExp(`^${input}`,'i')
      suggestions = this.state.categories.sort().filter(v => regExp.test(v.name))
    }
    this.setState(() => ({suggestions, input_text: input}))
  }

  // return all the results
  returnResults() {
    const suggestions = this.state.suggestions
    return(suggestions.length!==0? 
          <ul> 
            {suggestions.map((item => <li key={item.id} onClick={()=> this.onClickSearchResult(item)}>{item.name}</li>))} 
          </ul> 
          : 
          null)
  }

  // on click a search result checks if the item already exists in the selected list, if it does not it pushes it
  // also it clears suggestion border and input text
  onClickSearchResult (item) {
    console.log("Clicked on " + item);

    if(this.state.selected.includes(item) === false) {
      this.props.update("categories", item)
      this.setState({ selected: [...this.state.selected, item] })
    }
    this.setState({
      input_text: "",
      suggestions: [] 
    })
  }

  // remove an item from the selected list
  onClickRemoveItem = event => {
    const itemID = parseInt(event.target.id)

    // hold all the items except the one we want to delete
    let newArray = this.state.selected.filter(item => item.id !== itemID )
    this.props.update("categories", {id: itemID})
    this.setState({ selected: newArray })
  }

  render() {
    // get selected items
    const selected = this.state.selected.map(item => { 
      return (<tr key={item.id}>
          <td>{item.name}</td>
          <td> 
            <img 
              id={item.id} 
              className="img-responsive m-3 remove-logo" 
              src={RemoveLogo} 
              height="10px"
              width="10px" 
              alt="logo" 
              onClick={this.onClickRemoveItem} /> 
          </td>
        </tr>
      ) 
    })
    // fix the table 
    const table = <table className="table table-striped custab" >
                    <thead >
                      <tr >  
                        <td><b>Category</b></td>
                        <td><b>Action</b></td>
                      </tr>
                    </thead>
                    <tbody>
                      { selected }
                    </tbody>
                </table> 

    return (
      <div>
        <div className="AutoCompleteSelection">
          <input 
            value={this.state.input_text} 
            type="search" 
            autoComplete="off"
            className="form-control" 
            name="categories" 
            placeholder={"Enter categories"} 
            onChange={this.onInputChange} />
          {this.returnResults()}
        </div>
        <div className="AutoCompleteSelectionSelected"> <br/> <br/>
          {this.state.selected.length > 0 ? table : null }  {/* show the table only if there is at least one selected item */}
        </div>
      </div>
    )
  }
}

export default AutoCompleteSelection