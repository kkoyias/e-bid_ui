import React from 'react'
import { ListGroup } from "react-bootstrap"
import { formatDate } from "../../Utils/Utils"
import Avatar from "@material-ui/core/Avatar"

function ChatList(props) {
    const myID = parseInt(localStorage.getItem('userId'))

    return (
            <ListGroup>
            {
                props.chatList.map((chat, index) => {
                  const limit = 15, peers = chat.participants.filter(participant => participant.id !== myID)
                  const userName = peers.length === 0 ? "Unknown" : peers[0].userName.slice(0, limit)
                  const lastMessage = chat.messages.length === 0 ? 
                    ">> Start a chat with the provider '" + userName + "'" 
                      : 
                    (chat.messages[chat.messages.length-1].sender.id === myID ? "You: " : "")
                    + chat.messages[chat.messages.length-1].content.slice(0, limit*2) 
                    + (chat.messages[chat.messages.length-1].content.length > 2 * limit ? "..." : "")

                  return (
                    <div className="chat_divider" key={chat.id} onClick={() => props.handler(index, userName)}>
                        <div className="chat_list_image">
                          {/* get first username letter and fix the image */}
                          <Avatar>{userName[0]}</Avatar>
                        </div>
                        <div className="chat_header">
                          {/* get username */}
                          <h5 className="chat_name">{userName}
                            <span className="chat_notification">{formatDate(new Date(chat.lastModified))}</span>
                          </h5>
                          {/* get last message and show it's suffix or prompt to Say hi if there is no message yet*/}
                          <p>{lastMessage}</p>
                        </div>
                    </div>
                    )})
            }
            </ListGroup>
    )
}

export default ChatList