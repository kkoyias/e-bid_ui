import React from 'react'
import ChatList from "./ChatList"
import ChatConversation from "./ChatConversation"
import NewMessage from "./NewMessage"
import {authorizeUser} from "../../Utils/Utils"
import { Redirect } from "react-browser-router"
import "./rsrc/chat.css"

const baseUrl = "http://localhost:8080/api"

class Chat extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedChat: null,
            chatList: [],
            peer: "",
            providerID: parseInt(this.props.match.params.id),
            myID: parseInt(localStorage.getItem('userId'))
        }
        this.handler = this.handler.bind(this)
        this.getChats = this.getChats.bind(this)
        this.submitMessage = this.submitMessage.bind(this)
        this.haveChattedWith = this.haveChattedWith.bind(this)
        this.startChat = this.startChat.bind(this)
    }

    componentDidMount() {
        // initially, fetch all chats
        this.getChats(true)
    }

    // get the chat list or just the selected one
    getChats(startChat){
        const {chatList, selectedChat, myID} = this.state, chatID = selectedChat !== null ? chatList[selectedChat].id : -1
        const url = baseUrl + (chatID === -1 ? "/clients/" + myID + "/chats" : "/chats/" + chatID)

        // fetch the chat list or the just updated chat
        fetch(url, {
            method: "GET"
        })
        // response
        .then(httpResponse => httpResponse.json())
            .then(jsonResponse => {

                // if all chats are to be updated, do it
                if(selectedChat === null)
                    this.setState({ chatList: jsonResponse})

                // else if only one chat was updated/refreshed
                else{
                    const {chatList} = this.state, chatID = chatList[selectedChat].id

                    //update just the chat reloaded
                    chatList[selectedChat] = jsonResponse

                    // sort chats by descending modification date in order to adjust the chat list
                    chatList.sort((chatA, chatB) => new Date(chatB.lastModified) - new Date(chatA.lastModified))

                    this.setState({
                        // but keep selected chat as it is
                        selectedChat: chatList.map(chat => chat.id).indexOf(chatID),
                        chatList: chatList
                    })
                }

                // if user was redirected here after making a bid, start a chat with the provider if none exists yet
                if(startChat && this.state.providerID > 0 && !this.haveChattedWith())
                    this.startChat()

            })
        // reject
        .catch(error => console.error("Chat Fetch Error: ", error))
    }

    startChat(){
        if(this.state.providerID > 0 && !this.haveChattedWith(this.state.providerID)){
            fetch(baseUrl + "/clients/" + this.state.myID + "/chats/" + this.state.providerID, {
                method: "POST",
                headers: new Headers({
                "Content-Type": "application/json"
                }),
                body: JSON.stringify(new Date())
            })
            // after getting a response, refresh chats and open chat-0, the one just created
            .then(response => this.getChats(false))
            .catch((error) => console.error("Error creating new chat: ", error))
        }
    }

    haveChattedWith(){
        for(let i = 0; i < this.state.chatList.length; i++){

            // if the provider is a participant of this chat then they already have one
            for(let j = 0; j < this.state.chatList[i].participants.length; j++){

                if(this.state.chatList[i].participants[j].id === this.state.providerID){
                    console.log("There has already been a chat with " + this.state.providerID + " (" + i + ")")
                    this.setState({selectedChat: i})
                    return true
                }                
            }
        }
        return false
    }

    // save the index of the chosen chat
    handler = (index, peer) => {this.setState({ selectedChat: index, peer: peer})}

    // send the message
    submitMessage = (message) => {
        console.log("Submitting")
        const {chatList, selectedChat, myID} = this.state, chatID = chatList[selectedChat].id
        fetch(baseUrl + "/clients/" + myID + "/chats/" + chatID + "/messages", {
            method: "POST",
            headers: new Headers({
            "Content-Type": "application/json"
            }),
            body: JSON.stringify({content: message, timeStamp: new Date()})
        }) 
        // once a response is delivered, refresh this chat
        .then(httpResponse => this.getChats(false))
        .catch(error => console.error(error))
    }

    render() {

        // allow access to clients only
        const location = authorizeUser("ROLE_CLIENT")
        if(location !== "")
            return <Redirect to={location}/>

        const props = {...this.state, handler: this.handler}
        console.log(this.state)

        return (
            <div className="py-5">
                <div className="chat_border">
                    <div className="chat_list_scroll">
                        <ChatList {...props} />
                    </div>
                    {/* if the user has not selected a chat then don't show any conversation or new message to send */}
                    {this.state.selectedChat !== null && 
                        <div className="conversation">
                            <div className="conversation_scroll">
                                <ChatConversation {...this.state} />
                            </div>
                            <NewMessage refresh={() => this.getChats(false)} submitMessage={this.submitMessage}/>
                        </div> }
                </div>
            </div>
        )
    }
}

export default Chat