import React from 'react'
import { formatDate } from "../../Utils/Utils"
import Avatar from "@material-ui/core/Avatar"

function ChatConversation(props){
    const chat = props.chatList[props.selectedChat], myID = parseInt(localStorage.getItem('userId'))
    const data = {
        self: {
            className: "user_message", 
            avatar: null
        }, 
        other: {
            className: "incoming_message", 
            avatar: (name) => <div className="conversation_image"><Avatar>{name}</Avatar></div>
        }
    }

    const date = new Date()
    console.log(date)
    date.setHours(date.getHours() + 3)
    console.log(date)
    return (
        <div>
            {chat.messages.map((message,index) => {
                    const meta = data[(message.sender.id === myID ? "self" : "other")]
                    return (
                        <div key={index}>
                                    <div>
                                        <h6 id="message-date">{formatDate(new Date(message.timeStamp))}</h6>
                                        {meta.avatar && meta.avatar(message.sender.userName[0])}
                                        <div className={meta.className}>
                                            <p 
                                                className={meta.className + "_text"}>
                                                {message.content}<br/>
                                            </p>
                                        </div>
                                    </div>
                        </div>
                    )
                }) 
            }
        </div>
    )
}

export default ChatConversation