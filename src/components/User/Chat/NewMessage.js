import React from 'react'
import Send_Message_Logo from "./rsrc/send-message.png"

class NewMessage extends React.Component {
    constructor(props) {
        super(props)
        this.state = {message: ""}
    }

    sendMessage = () => {
        // if the message is not empty, pass it to parent/Chat.js and clear current input
        if(this.state.message.length !== 0) {
            this.props.submitMessage(this.state.message)
            document.getElementById("new_message").value = ""
            this.setState({ message: "" })
        }
    }

    render() {

        return (
            <div>
                <input 
                    id="new_message" 
                    className="new_message" 
                    onChange={(e) => this.setState({message: e.target.value})}
                    onKeyPress={(e) => e.which === 13 && this.sendMessage()} 
                    value={this.state.message}
                    type="text" 
                    placeholder="Type a new message..." 
                />
                <div className="new_message_icons">
                    <i className="fas fa-sync fa-lg" onClick={this.props.refresh}></i>                
                    <img 
                        src={Send_Message_Logo} 
                        onClick={this.sendMessage} 
                        alt="logo" 
                    />
                </div>
            </div>
        )
    }
}

export default NewMessage