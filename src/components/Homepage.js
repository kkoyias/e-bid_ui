import React from 'react'
import ReactDOM from 'react-dom'

import {Redirect} from 'react-browser-router'
import {authorizeUser} from './Utils/Utils'
import { Load, itemIsActive, getProducts } from './Utils/Utils'
import AlertBox from './Utils/AlertBox/AlertBox'
import Listing from "./Results/Listing"

const config = {prodsPerRow : 4, useRows : 2}
const filterItems = (item => itemIsActive(item) && parseInt(localStorage.getItem('userId')) !== item.seller.id)

class Homepage extends React.Component {
  constructor() {
    super()
    this.state = {products: [], loading: true}
  }

  // on mounting fetch recommendations to display
  componentDidMount(){
      ReactDOM.findDOMNode(this).addEventListener('click', () => this.setState({alerts: []}))

      const url = "http://localhost:8080/api/clients/" + localStorage.getItem('userId') + "/recommendations"
      fetch(url)
          .then(response => response.json())
            .then(data => this.setState({
                loading: false,

                // create product list out of the result, omitting expired offers
                products : data.filter(filterItems)
                  .map((item) => {return {...getProducts(item), recommended: true}})
            }))
          .catch(error => {
                this.setState({
                    loading: false,
                    alerts: [{
                        message: "Failed to load resource. Please check your connection",
                        type: "danger"
                    }]
                })
                console.error("Error on welcome fetch: ", error)
            })
  }

  render() {
    console.log(this.state)

    const location = authorizeUser("ROLE_CLIENT") 
    if(location !== "")
      return <Redirect to={location} />

    return (
        <div>
          <div id="home-image" className="display-4">
              <div id="home-text">
                  <h1>{"Bid, buy, sell & have fun"}</h1>
              </div>
          </div><br/>
          <h3 className="main-header">Based on your profile</h3><br/>
          <div>{this.state.loading ?  <Load/> : <Listing mode="check" list={this.state.products} config={config}/>}</div>
          {this.state.alerts && <AlertBox state={this.state} handler={() => this.setState({alerts: []})}/> }
        </div>
    )
  }
}

export default Homepage